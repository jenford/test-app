1.CycloneDX Maven Plugin
=========

The CycloneDX Maven plugin creates an aggregate of all direct and transitive dependencies of a project 
and creates a valid CycloneDX SBOM. CycloneDX is a lightweight software bill of materials 
(SBOM) specification designed for use in application security contexts and supply chain component analysis.

Maven Usage
-------------------
Insert this plugin into file ``pom.xml`` and run command ``mvn groupId:artifactId:version:goal`` 

```xml
<plugins>
   <plugin>
        <groupId>org.cyclonedx</groupId>
        <artifactId>cyclonedx-maven-plugin</artifactId>
        <version>2.5.3</version>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>makeAggregateBom</goal>
                </goals>
            </execution>
        </executions>
        <configuration>
            <projectType>library</projectType>
            <schemaVersion>1.3</schemaVersion>
            <includeBomSerialNumber>true</includeBomSerialNumber>
            <includeCompileScope>true</includeCompileScope>
            <includeProvidedScope>true</includeProvidedScope>
            <includeRuntimeScope>true</includeRuntimeScope>
            <includeSystemScope>true</includeSystemScope>
            <includeTestScope>false</includeTestScope>
            <includeLicenseText>false</includeLicenseText>
            <outputFormat>all</outputFormat>
            <outputName>bom</outputName>
        </configuration>
    </plugin>
</plugins>
```

Excluding Projects
-------------------
With `makeAggregateBom` goal it is possible to exclude certain Maven Projects (artifactId) from getting included in bom.

* Pass `-DexcludeTestProject=true` to skip any maven project artifactId containing the word "test"
* Pass `-DexcludeArtifactId=comma separated id` to skip based on artifactId

Goals
-------------------
The CycloneDX Maven plugin contains the following three goals:
* makeBom
* makeAggregateBom
* makePackageBom

By default, the BOM(s) will be attached as an additional artifacts during a Maven install or deploy.

* `${project.artifactId}-${project.version}-cyclonedx.xml`
* `${project.artifactId}-${project.version}-cyclonedx.json`

This may be switched off by setting `cyclonedx.skipAttach` to true.

makeBom and makeAggregateBom can optionally be skipped completely by setting `cyclonedx.skip` to true.

2.CycloneDX Node.js Module
=========

The CycloneDX module for Node.js creates a valid CycloneDX Software Bill-of-Materials (SBOM) containing an aggregate of all project dependencies. CycloneDX is a lightweight SBOM specification that is easily created, human and machine readable, and simple to parse.

Requirements
-------------------
Global Node.js v12.0.0 or higher

Usage
-------------------

#### Installing

```bash
npm i @cyclonedx/bom

npm install -g @cyclonedx/bom
```

#### Getting Help
```bash
$ cyclonedx-bom -h
Usage: cyclonedx-bom [OPTIONS] [path]

Creates CycloneDX Software Bill-of-Materials (SBOM) from Node.js projects

Options:
  -v, --version              output the version number
  -d, --include-dev          Include devDependencies (default: false)
  -l, --include-license-text Include full license text (default: false)
  -o, --output <output>      Write BOM to file (default: "bom.xml")
  -t, --type <type>          Project type (default: "library")
  -ns, --no-serial-number    Do not include BOM serial number
  -h, --help                 display help for command
```

#### Example (XML)
```bash
cyclonedx-bom -o bom.xml
```

#### Example (JSON)
```bash
cyclonedx-bom -o bom.json
```

3.Integration

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/ezqp35iZZTk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
