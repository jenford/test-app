pipeline{
    agent any
    stages{
        stage("Multi-GitSCM") {
            steps {
                checkout([
                    $class: 'GitSCM',
                    branches: [[name: "*/master"]],
                    userRemoteConfigs: [[credentialsId: 'Git-Lab', url: 'git@git.strikersoft.com:tetra/tetra-operational-web.git']]
                ])
            }
        }
        stage("Build"){
            steps {
                script{
                    def scannerHome = tool name: 'SonarQube', type: 'hudson.plugins.sonar.SonarRunnerInstallation';
                    withSonarQubeEnv('SonarQube') {
//                         sh """
//                          ${scannerHome}/bin/sonar-scanner \
//                         -D sonar.projectVersion=1.0-SNAPSHOT \
//                         -D sonar.login=admin \
//                         -D sonar.java.binaries=web/src/main/java/ \
//                         -D sonar.password=qwaszx12 \
//                         -D sonar.projectKey=tetra-operational-web \
//                         -D sonar.sourceEncoding=UTF-8 \
//                         -D sonar.exclusions=web/src/main/webapp/vendor/** \
//                         -D sonar.sources=. \
//                         -D sonar.host.url=http://192.168.108.100:9000/"""

                    }

                }
            }
        }
//         stage("Compile-Package"){
//             steps{
//                 script{
//                     def mvnHome = tool name: 'Maven', type: 'maven'
//                     sh "${mvnHome}/bin/mvn package -Dcheckstyle.skip=true"
//                 }
//             }
//         }
//         stage("SonarQube Analysis") {
//             steps{
//                 script{
//                 def mvnHome = tool name: 'Maven', type: 'maven'
//                 withSonarQubeEnn('SonarQube') {
//                     sh "${mvnHome}/bin/mvn sonar:sonar"
//                 }}
//             }
//         }
    }
    post {
        always {
            deleteDir()
        }
    }
    options {
      buildDiscarder(logRotator(numToKeepStr: '5'))
    }

}
